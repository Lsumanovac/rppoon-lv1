using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV1
{
    class Note
    {
        private String author;
        private String text;
        private int priority;

        public string getAuthor() { return this.author; }
        public string getNote() { return this.text; }
        public int getPriority() { return this.priority; }
        public void setNote(string newNote) { this.text = newNote; }
        public void setPriority(int newPriority) { this.priority = newPriority; }

        public Note(string newText)
        {
            this.author = "Anonymus";
            this.text = newText;
            this.priority = 1;
        }
        public Note(string newText, string newAuthor)
        {
            this.author = newAuthor;
            this.text = newText;
            this.priority = 1;
        }
        public Note(string newText, string newAuthor, int newPriority)
        {
            this.author = newAuthor;
            this.text = newText;
            this.priority = newPriority;
        }

        public string Text { get; set; }
        public string Author { get; private set; }
        public int Priority { get; set; }

        public override string ToString()
        {
            return this.author + ": " + this.text;
        }

    }
}
