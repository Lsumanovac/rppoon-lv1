using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV1
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note1, note2, note3;
            note1 = new Note("Text for note 1.");
            note2 = new Note("Text for note 2.", "Leon Sumanovac");
            note3 = new Note("Text for note 3.", "Leon Sumanovac", 1);

            Console.WriteLine(note1.getAuthor());
            Console.WriteLine(note1.getNote());
            Console.WriteLine(note2.getAuthor());
            Console.WriteLine(note2.getNote());
            Console.WriteLine(note3.getAuthor());
            Console.WriteLine(note3.getNote());
        }
    }
}
