using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV1
{
    class NoteWithTime : Note
    {
        private DateTime timeWhenMade;

        public NoteWithTime(string newText) : base(newText)
        {
            timeWhenMade = DateTime.Now;
        }
        public NoteWithTime(string newText, string newAuthor) : base(newText, newAuthor)
        {
            timeWhenMade = DateTime.Now;
        }
        public NoteWithTime(string newText, string newAuthor, int newPriority) : base(newText, newAuthor, newPriority)
        {
            timeWhenMade = DateTime.Now;
        }
        public NoteWithTime(string newText, string newAuthor, int newPriority, DateTime newTime) : 
            base(newText, newAuthor, newPriority)
        {
            this.timeWhenMade = newTime;
        }

        public string getTime()
        {
            return timeWhenMade.ToString();
        }
        public void setTime(DateTime newTime)
        {
            this.timeWhenMade = newTime;
        }

        public override string ToString()
        {
            return base.ToString() + " " + this.timeWhenMade.ToString();
        }
    }
}
